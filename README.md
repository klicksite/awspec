# AWSpec

Este pipe auxilia na execução de testes utilizando o [awspec](https://github.com/k1LoW/awspec)

![pipe](pipe.svg)

---
## Variáveis

*( * )Obrigatório*

| variavel                  | padrão    | descrição              |
|:--------------------------|:---------:|:-----------------------|
| (*) AWS_ACCESS_KEY_ID     |           | AWS access key         |
| (*) AWS_SECRET_ACCESS_KEY |           | AWS secret             |
| ( ) AWS_DEFAULT_REGION    | us-east-1 | AWS region             |
| ( ) COMPARATION_BRANCH    | master    | Branch para comparação |
---
## Utilização

```bash
script:
  - pipe: klickpages/awspec:latest
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
```
